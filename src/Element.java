public class Element<T> implements Measurable{
    private T obj;

    public Element(T obj){
        this.obj = obj;
    }

    @Override
    public double getMeasure(){
        if(obj instanceof Integer)
            return((Integer)((Object)obj));
        else
            return ((double)((Object)obj));

    }
}
