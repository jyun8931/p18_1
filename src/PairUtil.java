public class PairUtil<E, T> {

    private Measurable obj1;
    private Measurable obj2;

    public PairUtil(){
        this.obj1 = this.obj2 = null;
    }

    public PairUtil(Measurable obj1, Measurable obj2){
        this.obj1 = obj1;
        this.obj2 = obj2;
    }

    public Measurable getObj1() {
        return obj1;
    }

    public Measurable getObj2() {
        return obj2;
    }

    public static <E, T> PairUtil<E, T> minmax(Element[] elements){
        double min = (Double)elements[0].getMeasure();
        int minIndex = 0;
        for(int i = 0; i < elements.length; i++){
            if(elements[i] != null && elements[i].getMeasure() < min){
                min = elements[i].getMeasure();
                minIndex = i;
            }
        }
        Element<E> minEl = elements[minIndex];

        double max = (Double)elements[0].getMeasure();
        int maxIndex = 0;
        for(int i = 0; i < elements.length; i++){
            if(elements[i] != null && elements[i].getMeasure() > max){
                max = elements[i].getMeasure();
                maxIndex = i;
            }
        }
        Element<T> maxEl = elements[maxIndex];

        PairUtil<E, T> pair = new PairUtil<>(minEl, maxEl);
        return pair;
    }

    @Override
    public String toString()
    {
        return("(" + obj1.getMeasure() + ", " + obj2.getMeasure() + ")");
    }
}