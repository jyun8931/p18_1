
public class P18_1 {
    public static void main(String[] args) {
        Element[] elements = new Element[5];
        elements[0] = new Element<Integer>(3);
        elements[1] = new Element<Double>(5.5);
        elements[2] = new Element<Double>(99.0);
        elements[3] = new Element<Integer>(1);

        PairUtil<Double, Double> pair = PairUtil.minmax(elements);
        System.out.println("The min-max pair is: " + pair);
    }
}

